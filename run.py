import argparse
import logging
import time
import pyedflib

from eeg_port import list_serial_ports, EEGPort, EEGPacket


logger = logging.getLogger("App")


def read_n_packets(port, num_packets=0):
    packets = []

    logger.info("Reading {} packets from port".format(num_packets))

    while (num_packets > 0):
        packet = port.read()
        if (packet != None):
            logger.debug(packet)
            packets.append(packet)
            num_packets -= 1

    return packets


def read_n_seconds(port, num_seconds=0):
    packets = []

    logger.info("Reading {} seconds from port".format(num_seconds))

    end_time = time.time() + num_seconds

    while (time.time() <= end_time):
        packet = port.read()
        if (packet != None):
            logger.debug(packet)
            packets.append(packet)

    return packets



def main():
    parser = argparse.ArgumentParser("An app to read from an openEEG device")

    p = parser.add_argument_group("Serial")
    p.add_argument("--port", "-p", type=str, default=None, help="The serial port to connect to")
    p.add_argument("--baudrate", "-b", type=int, default=57600, help="The serial port baudrate (default %(default)s)")
    p.add_argument("--list-ports", "-l", action="store_true", help="List available serial ports")

    r = parser.add_argument_group("Reader")
    r.add_argument("--num-packets", type=int, default=None, help="Read N packets")
    r.add_argument("--num-seconds", type=int, default=None, help="Read during N seconds")
    r.add_argument("--file-name", "-f", type=str, default="openeeg.edf", help="The .edf file to store data into (default %(default)s)")

    l = parser.add_argument_group("Logging")
    l.add_argument("--log-level", type=str, default="DEBUG", help="Log level (default %(default)s)")
    l.add_argument("--log-format", type=str, default="[ %(levelname)7s ] %(name)-16s   >   %(funcName)-20s  :  %(message)s", help="Log format")

    args = parser.parse_args()
    logging.basicConfig(format=args.log_format, level=args.log_level)
    
    logger.info("Hello")

    if (args.list_ports):
        list_serial_ports()
        return

    if (args.port):
        eeg = EEGPort(args.port, args.baudrate)

        # Read from port
        if (args.num_packets):
            packets = read_n_packets(eeg, args.num_packets)
        elif (args.num_seconds):
            packets = read_n_seconds(eeg, args.num_seconds)
        else:
            logger.error("Please specify a reader argument '--num-packets' or '--num-seconds'")
            eeg.close()
            return

        # Write to file
        logger.info("Writing to file {}".format(args.file_name))
        # signals = list(map(list, zip(*packets)))
        signals = [s.channels for s in packets]
        channel_names = ["ch1", "ch2", "ch3", "ch4", "ch5", "ch6"]
        signal_headers = highlevel.make_signal_headers(channel_names, sample_frequency=EEGPacket.SAMPLE_RATE)
        header = highlevel.make_header(patientname='patient_x', gender='Male')
        highlevel.write_edf(args.file_name, signals, signal_headers, header)
        # print("--------------------------------------")
        # c = pyedflib.highlevel.read_edf(args.file_name)
        # print(c)

        eeg.close()
    else:
        logger.error("You must specify a port with '--port' argument")

    logger.info("Bye")







if __name__ == "__main__":
    main()

