# OpenEEG

A python tool to interact with an OpenEEG serial device

## Development

- Requires python 3.9+:

```shell
brew install python3
```

- Pip requirements:

```shell
pip install -r requirements.txt
```


## Usage

To get the list of available ports:

```shell
python run.py --list-ports
```

To run:

```shell
python run.py -p <port name>
```

To get all available arguments:

```shell
python run.py --help
```


## Todo

- Better port handling and data sync mechanism
- Support for V3 Firmwares and packet types
- Check if performance is enough with current packet rate


## References

- OpenEEG [project](https://openeeg.sourceforge.net/doc/index.html).
- Olimex OpenEEG SMT [Product page](https://www.olimex.com/Products/EEG/OpenEEG/EEG-SMT/open-source-hardware).
- Olimex OpenEEG SMT [manual](https://www.olimex.com/Products/EEG/OpenEEG/EEG-SMT/resources/EEG-SMT.pdf).
- Olimex OpenEEG SMT [AVR firmware v2](https://www.olimex.com/Products/EEG/OpenEEG/EEG-SMT/resources/EEG-SMT_firmware.zip).

- MNE python tool: [documentation](https://mne.tools/stable/index.html#) [source](https://github.com/mne-tools/mne-python).
- Brainflow: [source](https://github.com/brainflow-dev/brainflow).
