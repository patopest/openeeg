import serial
from serial.tools import list_ports
import logging

logger = logging.getLogger(__name__)

def list_serial_ports():
    logger.info("Available serial ports:")
    for port in list_ports.comports():
        print("\t - {}".format(port))


"""
////////// Packet Format Version 2 ////////////

// 17-byte packets are transmitted from the ModularEEG at 256Hz,
// using 1 start bit, 8 data bits, 1 stop bit, no parity, 57600 bits per second.

// Minimial transmission speed is 256Hz * sizeof(modeeg_packet) * 10 = 43520 bps.

struct modeeg_packet
{
    uint8_t     sync0;      // = 0xa5
    uint8_t     sync1;      // = 0x5a
    uint8_t     version;    // = 2
    uint8_t     count;      // packet counter. Increases by 1 each packet.
    uint16_t    data[6];    // 10-bit sample (= 0 - 1023) in big endian (Motorola) format.
    uint8_t     switches;   // State of PD5 to PD2, in bits 3 to 0.
};

// Note that data is transmitted in big-endian format.
// By this measure together with the unique pattern in sync0 and sync1 it is guaranteed, 
// that re-sync (i.e after disconnecting the data line) is always safe.

"""

class EEGPacket:
    # Statics
    RAW_PACKET_SIZE     = 17
    SYNC0               = 0xa5
    SYNC1               = 0x5a
    NUM_CHANNELS        = 6
    SAMPLE_RATE         = 256

    # Indexes
    SYNC0_IDX           = 0
    SYNC1_IDX           = 1
    VERSION_IDX         = 2
    COUNT_IDX           = 3
    CHANNELS_START_IDX  = 4
    CHANNELS_END_IDX    = 15
    SWITCHES_IDX        = 16

    def __init__(self, buffer):
        self._number = 0
        self._channels = []
        self._switches = 0
        self._raw = buffer

        self.valid = self._parse(buffer)


    @property
    def raw(self):
        return self._raw

    @property
    def version(self):
        return self._version
    
    @property
    def number(self):
        return self._number

    @property
    def channels(self):
        return self._channels

    @property
    def switches(self):
        return self._switches
    
    
    def get_channel(self, index):
        if (index < self.NUM_CHANNELS):
            return self._channels[index]
        else:
            raise IndexError("Channel index out of range")


    def _parse(self, buffer):
        self._version = buffer[self.VERSION_IDX]
        if (self._version != 2):
            logger.error("Unhandled packet version {}".format(self._version))
            return False

        if (len(buffer) != self.RAW_PACKET_SIZE):
            logger.error("Incorrect packet buffer size {} != {}".format(len(buffer), self.RAW_PACKET_SIZE))
            return False

        self._number = buffer[self.COUNT_IDX]

        i = 0
        channels = buffer[self.CHANNELS_START_IDX : self.CHANNELS_END_IDX + 1]
        for j in range(self.NUM_CHANNELS):
            high_byte = channels[i]
            low_byte  = channels[i + 1]

            value = (high_byte << 8) | (low_byte & 0xFF)
            self._channels.append(value)

            i += 2

        self._switches = buffer[self.SWITCHES_IDX]

        return True


    def __str__(self):
        s = "number: {}, channels: {}, sws: {}".format(self.number, self.channels, self.switches)
        return s



class EEGPort:
    port = None
    logger = None

    def __init__(self, port, baudrate=57600):
        self.logger = logging.getLogger(self.__class__.__name__)   
        self.synced = False 

        try:
            self.port = serial.Serial(port, baudrate, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)
            self.logger.info("Succesfully opened port: {}".format(port))
        except Exception as e:
            self.logger.error("Unable to open port {}: {}".format(port, e))


    def read_raw(self, size=EEGPacket.RAW_PACKET_SIZE):
        if self.port:
            data = self.port.read(size=size)
            data = [int(a) for a in data]
            return data
        else:
            self.logger.error("Cannot read port")
            return None


    def read_and_sync(self):

        data = self.read_raw()
        if (data == None):
            return None

        offset = 0
        start_found = False
        for i in range(EEGPacket.RAW_PACKET_SIZE):                
            if ((data[i] == EEGPacket.SYNC0) and (data[i + 1] == EEGPacket.SYNC1)):
                offset = i
                start_found = True

        if (start_found == False):
            self.logger.warning("Unable to found start packet sequence [0xa5, 0x5a, 0x02]!")
            return self.read_and_sync() # not great but it seems to work since byte errors are quite low      

        start_data = data[offset:]

        data = self.read_raw(size=offset)
        packet = start_data + data

        self.synced = True

        return packet


    def read(self):
        if self.synced:
            buffer = self.read_raw()
        else:
            buffer = self.read_and_sync()

        if (buffer):
            packet = EEGPacket(buffer)
            if (packet.valid):
                return packet
            
        return None


    def close(self):
        if self.port:
            self.logger.info("Closing port {}".format(self.port.name))
            self.port.close()






