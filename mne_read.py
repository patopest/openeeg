import os
import numpy as np
import matplotlib.pyplot as plt
import mne
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
# the following import is required for matplotlib < 3.2:
from mpl_toolkits.mplot3d import Axes3D  # noqa
import sklearn
from autoreject import get_rejection_threshold



################################################################################


def read_ch(file_name, time):
	channels = ["ch1","ch2","ch3","ch4","ch5","ch6"]
	file = mne.io.read_raw_edf(file_name, stim_channel=channels,preload = True)
	file.plot_psd(fmax=50)
	return file, time

def set_ref(file):
	mne.set_bipolar_reference(file,"ch1","ch2",ch_name="ref")

def montage(file):
	# --- Get coordinates for MNI --- 
	# --- https://bioimagesuiteweb.github.io/webapp/mni2tal.html ---
	#builtin_montages = mne.channels.get_builtin_montages(descriptions=True)
	#easy_montage = mne.channels.make_standard_montage('easycap-M1')
	#file.set_montage('easycap-M1')
	#fig = file.plot_sensors(show_names=True)	
	#montage = mne.channels.read_montage(kind='filename', ch_names=None, path='datapath', unit='m', transform=False)
	#positions = {'CH_0' : 'EEG 001', 'CH_1' : 'EEG 002', 'CH_2': 'EEG 003', 'CH_3': 'EEG 004', 'CH_4'  : 'EEG 005', 'CH_5'  : 'EEG 006'}
	positions = {'CH_0': [-57, 0, 42], 'CH_1': [57, 0, 42], 'CH_2': [65, 11, 0], 'CH_3': [-65, 11, 0], 'CH_4': [8, 11, 71],'CH_5': [-8, 11, 71]}
	
	montage = mne.channels.make_dig_montage(ch_pos=positions, nasion=[0, -24, -4], lpa=None, rpa=None, hsp=None, hpi=None, coord_frame='mni_tal')
	file.set_montage(montage, match_case=True, match_alias=False, on_missing='raise', verbose=None)



################################################################################

def plot_freq(file, fmax=50):
	file.plot_psd(fmax=50)

def olimex_filter(file):
	filtered = f.filter(l_freq=15, h_freq=45, h_trans_bandwidth=0.1)
	return filtered

def ica(file,n_components,random_state,max_iter):
	ica = mne.preprocessing.ICA(n_components=20, random_state=97, max_iter=800)
	ica.fit(file)
	ica.exclude = [1, 2]  # details on how we picked these are omitted here
	ica.plot_properties(raw, picks=ica.exclude)


def FFT(file, time, channels="CH_1"):
	frequencies = mne.time_frequency.Spectrum(inst = file, method = 'welch', fmin=0, fmax=45, tmin=0, tmax=time, proj = False, reject_by_annotation = False, picks = channels, n_jobs = None)
	frequencies.plot()
	return frequencies

def FFTs(file, time, channels="CH_1"):
	frequencies = mne.time_frequency.EpochsSpectrum(inst = file, method = 'welch', fmin=0, fmax=45, tmin=0, tmax=time, proj = False, picks = "CH_1", n_jobs = None)
	frequencies.plot()
	return frequencies

def one_ch_oscilloscope(file, time, channel_index=1):
	start_stop_seconds = np.array([0, time])
	sampling_freq = file.info['sfreq']
	start_sample, stop_sample = (start_stop_seconds * sampling_freq).astype(int)
	raw_selection = f[channel_index, start_sample:stop_sample]
	x = raw_selection[1]
	y = raw_selection[0].T
	plt.plot(x, y)
	plt.show()
